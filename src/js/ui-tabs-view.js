/**
 * Created by zhang on 2017/6/7.
 */
import uiTabsModule from './ui-tabs';
import uiTabsHtml from '../tpls/ui-tabs.html';
import '../scss/ui-tabs.scss';
import './ui-tabs-menu';
// import './ui-tabs-drag';

uiTabsModule.directive('uiTabsView', function ($rootScope,$timeout, $controller, $compile, uiTabs) {
    return {
        restrict: 'EAC',
        priority: 400,
        replace: true,
        template: uiTabsHtml,
        link: function (scope, element,attr) {

            console.log("uiTabs",uiTabs);

            scope.tabs = uiTabs.tabs;
            scope.current = uiTabs.current;

            scope.$on('tabOpenStarting', tabOpenStarting);
            scope.$on('tabOpenSuccess', tabOpenSuccess);
            scope.$on('tabOpenError', tabOpenError);
            scope.$on('tabCloseSuccess', tabCloseSuccess);
            scope.$on('tabChangeSuccess', tabChangeSuccess);
            scope.$on('tabRefresh', tabRefresh);
            scope.$on('$destroy', uiTabs.closeAll); // 指令销毁时，清楚所有tab

            //创建默认tab
            createDefaultTab(attr);

            // 关闭tab-用于tab上面的关闭
            scope.close = function (e, tab) {
                console.log("this  is  close ....");
                tab.close();
                e.stopPropagation();
            };


            scope.refresh=function(e,tab,isAuto){
                tabRefresh(e, tab,isAuto);
            };

            // 切换到tab页
            scope.activeTab = function (tab) {
                uiTabs.active(tab);
            };

            // 右键菜单选中
            scope.menuSelect = function (tab, action) {

                switch (action) {
                    case 'refresh':
                        tab.refresh();
                        break;
                    case 'current':

                        if(scope.defaultName !== tab.name){
                            tab.close();
                        }
                        break;

                    case 'left':
                        closeLeft(tab);
                        break;
                    case 'right':
                        closeRight(tab);
                        break;

                    case 'other':

                        // closeLeft(tab);
                        // closeRight(tab);

                        closeOther(tab);

                        break;
                    case 'colseAll':
                        closeAll();
                        // createDefaultTab(attr);
                        break;    
                }

            };

            // 设置可拖动区域
            scope.opts = {
                containment: '.ui-tabs-nav'
            };

            //关闭其他标签页
            function closeOther(tab) {

                var closeTabs=[];
                var indexs=[];
                angular.forEach(uiTabs.tabs,function (closeTab,index) {
                    if(closeTab.id != tab.id && closeTab.name != uiTabs.defaultName){
                        indexs.push(index);
                        closeTabs.push(closeTab);
                    }
                });

                angular.forEach(closeTabs,function (item,index) {
                    item.close();
                });
            }



            /**
             * 关闭左侧标签  -默认页 除外
             * @param tab
             */
            function closeLeft(tab) {
                var closeIndex = 0,
                    closeTab;

                while ((closeTab = uiTabs.tabs[closeIndex]) !== tab) {
                    if (!closeTab.close()) {
                        closeIndex++;
                    }

                }
            }

            /**
             * 关闭右侧标签  -默认页 除外
             * @param tab
             */
            function closeRight(tab) {
                var closeIndex = uiTabs.tabs.length - 1,
                    closeTab;

                while ((closeTab = uiTabs.tabs[closeIndex]) !== tab) {
                    closeTab.close();
                    closeIndex--;
                }
            }


            //关闭所有tab -默认页 除外   
            function closeAll() {
                var list=[];
                var indexs=[];
                angular.forEach(uiTabs.tabs,function(closeTab,index){
                    if(closeTab.name != uiTabs.defaultName){
                        list.push(closeTab);
                        indexs.push(index);
                    }
                });

                angular.forEach(list,function (item,index) {
                    item.close();
                    // console.log("flag",flag);
                });

                // uiTabs.closeAll();
            }    

            /**
             * 当tab打开时，加载动画
             *
             * @param e
             * @param tab
             */
            function tabOpenStarting(e, tab) {
                scope.current = uiTabs.current;
                tab.loading = false;
            }

            /**
             * 当tab成功打开时，建立生成tab页页面内容，并且加载controller
             *
             * @param e
             * @param tab
             */
            function tabOpenSuccess(e, tab) {

                tab.loading = false; // 取消加载动画

                var newScope = tab.$scope = element.parent().scope().$new(),
                    link = $compile(tab.template),
                    pageNode = tab.$node = link(newScope),
                    container;

                if (tab.controller) {
                    // 实例controller，并且传入uiTabsParams 和 uiTab 参数
                    $controller(tab.controller, {
                        $scope: newScope,
                        uiTabsParams: tab.params || {},
                        uiTab: tab
                    });
                }

                $timeout(function () {
                    container = angular.element(element[0].querySelector('#ui-tabs-' + tab.id));
                    //先清空当前html 内容， 再追加；
                    container.html('');
                    container.append(pageNode);
                });
            }

            /**
             * 当tab加载失败，则取消动画
             *
             * @param e
             * @param tab
             */
            function tabOpenError(e, tab) {
                tab.loading = false;
            }

            /**
             * 当tab切换成功，则变更当前的tab页
             */
            function tabChangeSuccess() {
                scope.current = uiTabs.current;
            }

            /**
             * 当tab关闭时，销毁作用域，移除页面内容
             *
             * @param e
             * @param tab
             */
            function tabCloseSuccess(e, tab) {
                tab.$scope.$destroy();
                tab.$node.remove();
            }

            /**
             * 刷新tab页
             *
             * @param e
             * @param tab
             */
            function tabRefresh(e, tab,isAuto) {
                if (tab.$scope) {
                    tab.$scope.$destroy();
                }
                if (tab.$node) {
                    tab.$node.remove();
                }

                angular.extend(tab,{isAuto:isAuto})
                console.log("tabRefresh-tab",tab);
                $rootScope.$broadcast('manualRefresh', tab);
                tabOpenSuccess(e, tab);
            }

            /**
             * 创建默认tab
             * @param attr
             */
           function createDefaultTab(attr) {
                if(attr.defaultPage && attr.defaultName){

                    if(attr.defaultPage.indexOf('#/')){
                        attr.defaultPage=attr.defaultPage.replace('#/','views/');
                    }

                    uiTabs.open({
                        name:attr.defaultName,
                        templateUrl:attr.defaultPage
                    });

                    scope.defaultName=attr.defaultName;
                    uiTabs.defaultName=scope.defaultName;
                }
           }         


        }
    };
});